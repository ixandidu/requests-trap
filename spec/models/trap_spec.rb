require 'rails_helper'

RSpec.describe Trap, type: :model do
  context 'Validation' do
    it { is_expected.to validate_presence_of :name }
    it { is_expected.to validate_presence_of :slug }
    it { is_expected.to validate_uniqueness_of :slug }
  end

  context 'Relationship' do
    it do
     is_expected.to have_many(:requests).order(created_at: :desc).
                                         dependent(:destroy)
    end
  end
end
