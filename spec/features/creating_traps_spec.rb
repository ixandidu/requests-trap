require 'rails_helper'

feature 'Creating traps', type: :feature do
  let(:trap_name)  { 'Fireshop' }
  let(:saved_trap) { Trap.last }

  background { visit root_path }

  context 'with valid input' do
    background do
      fill_in  'Trap name', with: trap_name
      click_on 'Create Trap'
    end

    it 'redirect user to the received requests page' do
      expect(current_path).to eq("/#{saved_trap.slug}/requests")
      expect(page).to         have_text trap_name
    end

    it 'generate trap url based on the provided trap name' do
      expect(page).to have_field current_host, with: saved_trap.slug

    end

    describe 'customizing trap url' do
      context 'with non-empty input' do
        it 'sets the trap url with the provided input' do
          fill_in current_host, with: 'mysupersecreturlxj34'
          click_on 'Update Trap'

          expect(current_path).to eq('/mysupersecreturlxj34/requests')
          expect(page).to         have_field current_host, with: 'mysupersecreturlxj34'
        end
      end

      context 'with blank trap url' do
        it 'sets the trap url to the parameterized version its name' do
          fill_in current_host, with: ''
          click_on 'Update Trap'

          expect(current_path).to eq("/#{saved_trap.slug}/requests")
          expect(page).to         have_field current_host, with: saved_trap.slug
        end
      end
    end
  end

  context 'with invalid input' do
    background { click_on 'Create Trap' }

    it 're-render the homepage' do
      expect(current_path).to eq('/traps')

      expect(page).to have_text('Requests Trap')
      expect(page).to have_text('A tool for capture and display HTTP requests')
      expect(page).to have_text('Create your first trap and start capturing requests')
    end
  end
end
