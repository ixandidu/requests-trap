require 'rails_helper'

RSpec.feature 'Viewing received requests', type: :feature do
  let(:a_trap)         { Trap.create!(name: 'Fireshop') }
  let(:saved_request)  { a_trap.requests.last }
  let(:request_params) { {first: '1', second: '2nd'} }
  let(:browser)        { page.driver.browser }

  it 'list received requests' do
    # Capybara::RackTest::Driver instance can only do a GET, HEAD, POST, PUT,
    # and DELETE requests.
    %w[GET HEAD POST PUT DELETE].each do |http_method|
      browser.process(
        http_method.downcase,
        capture_requests_path(trap_id: a_trap),
        request_params
      )
    end

    visit requests_path(trap_id: a_trap)

    expect(page).to have_text('Received requests')

    a_trap.requests.each do |req|
      within("#request_#{req.id}") do
        expect(page).to have_text(req.headers['request']['date'])
        expect(page).to have_text(
          req.headers['request']['header']['REMOTE_ADDR']
        )
        expect(page).to have_text(
          req.headers['request']['header']['REQUEST_METHOD']
        )
      end
    end
  end

  scenario 'new received requests appear on the page in real time', js: true do
    visit requests_path(trap_id: a_trap)

    expect(page).to     have_text('(0)')
    expect(page).not_to have_text('POST')

    # Simulating a request outside of current session
    execute_script %($.post('#{capture_requests_path(trap_id: a_trap)}', {foo: 'bar'});)
    sleep 10 # wait for the requests fetcher script get executed

    expect(page).to have_text('(1)')
    expect(page).to have_text('POST')
  end

  feature 'Viewing individual request' do
    it 'display overview information of a request' do
      # create a dummy request
      browser.process('GET', capture_requests_path(trap_id: a_trap), request_params)
      request_time = Time.now.httpdate

      visit requests_path(trap_id: a_trap)
      expect(page).to have_text request_time
      click_on request_time

      expect(page).to have_text('Request Date')
      expect(page).to have_text('Remote IP')
      expect(page).to have_text('Request Method')
      expect(page).to have_text('Scheme')
      expect(page).to have_text('Query String')
      expect(page).to have_text('Query Params')
      expect(page).to have_text('Cookies')
    end

    it 'display request header', js: true do
      # create a dummy request
      visit root_path
      execute_script %($.post('#{capture_requests_path(trap_id: a_trap)}', {foo: 'bar'});)
      request_time = Time.now.httpdate

      visit requests_path(trap_id: a_trap)
      expect(page).to have_text request_time
      click_on request_time
      click_on 'Request Header'

      expect(page).to have_text('User-Agent')
    end

    it 'display raw response', js: true do
      # create a dummy request
      visit root_path
      execute_script %($.post('#{capture_requests_path(trap_id: a_trap)}', {foo: 'bar'});)
      request_time = Time.now.httpdate

      visit requests_path(trap_id: a_trap)
      expect(page).to have_text request_time
      click_on request_time
      click_on 'Raw Response'

      expect(page).to have_text('HTTP/1.1')
    end
  end
end
