require 'rails_helper'

feature 'Homepage', type: :feature do
  it 'display splash page with some instructions' do
    visit root_path

    expect(page).to have_text('Requests Trap')
    expect(page).to have_text('A tool for capture and display HTTP requests')
    expect(page).to have_text('Create your first trap and start capturing requests')
  end
end
