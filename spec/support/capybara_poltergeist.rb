require 'capybara/rails'
require 'capybara/poltergeist'

Capybara.register_driver :poltergeist do |app|
  Capybara::Poltergeist::Driver.new(app, {
    debug:             false,
    js_errors:         false,
    phantomjs_options: [
      '--load-images=no',
      '--ignore-ssl-errors=yes',
      '--disk-cache=yes',
      '--max-disk-cache-size=10000'
    ],
    inspector:        'open',
    timeout:          180,
    phantomjs_logger: false,
    logger:           false
  })
end

Capybara.javascript_driver = :poltergeist
