require 'rails_helper'

RSpec.describe 'Capturing trap requests', type: :request do
  let(:a_trap)         { Trap.create!(name: 'Fireshop') }
  let(:saved_request)  { a_trap.requests.last }
  let(:request_params) { {first: '1', second: '2nd'} }

  Request::HTTP_METHOD_TO_SUCCESS_STATUS_CODE.each do |http_method, status_symbol|
    context "#{http_method} request" do
      before :example do
        reset!
        integration_session.send(
          :process,
          http_method,
          capture_requests_path(trap_id: a_trap),
          request_params
        )
      end

      it "return #{status_symbol}" do
        expect(response.status).to eq(
          Rack::Utils::SYMBOL_TO_STATUS_CODE[status_symbol]
        )
      end

      it "save the request and response header in the db" do
        expect(saved_request.headers['response']['status']).to eq(
          response.status
        )

        expect(saved_request.headers['request']['header']).to eq(
          request.env.select { |key, _| key[/\A[A-Z0-9_]*\Z/] }
        )

        expect(saved_request.headers['response']['header']).to eq(
          response.headers
        )

        if http_method == 'GET'
          expect(saved_request.headers['request']['query_params']).to eq(
            request_params.stringify_keys
          )
        end

        if http_method == 'POST'
          expect(saved_request.headers['request']['body_params']).to eq(
            request_params.stringify_keys
          )
        end
      end
    end
  end
end
