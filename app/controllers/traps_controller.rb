class TrapsController < ApplicationController
  def new
    @trap = Trap.new
  end

  def create
    @trap = Trap.new(trap_params)
    if @trap.save
      redirect_to requests_path(trap_id: @trap)
    else
      render :new
    end
  end

  def update
    @trap = Trap.find(params[:id])
    @trap.update_attributes(trap_params)
    redirect_to requests_path(trap_id: @trap)
  end

  private
    def trap_params
      params.require(:trap).permit(:name, :slug)
    end
end
