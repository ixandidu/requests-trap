class RequestsController < ApplicationController
  skip_before_action :verify_authenticity_token

  helper_method :current_trap

  def index
    @requests = current_trap.requests
  end

  def show
    @request = current_trap.requests.find(params[:id])
  end

  def create
    headers[Request::Capturer::TRAP_ID] = current_trap.id.to_s
    head Request::HTTP_METHOD_TO_SUCCESS_STATUS_CODE[
      request.request_method
    ]
  end

  private
    def current_trap
      @current_trap ||= Trap.find(params[:trap_id])
    end
end
