class Trap < ActiveRecord::Base
  extend FriendlyId
  friendly_id :name

  validates_presence_of   :name, :slug
  validates_uniqueness_of :slug

  has_many :requests, ->{ order(created_at: :desc) }, dependent: :destroy
end
