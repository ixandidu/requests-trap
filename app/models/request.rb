class Request < ActiveRecord::Base
  # Resources:
  #
  #   * http://www.w3.org/Protocols/rfc2616/rfc2616-sec9.html
  #   * http://msdn.microsoft.com/en-us/library/aa143053(v=exchg.65).aspx
  #   * http://www.webdav.org/specs/rfc2518.html
  #
  HTTP_METHOD_TO_SUCCESS_STATUS_CODE = {
    'OPTIONS' => :no_content,
    'GET'     => :ok,
    'HEAD'    => :no_content,
    'POST'    => :created,
    'PUT'     => :no_content,
    'DELETE'  => :ok,
    'TRACE'   => :ok,
    'CONNECT' => :ok,

    # RFC2518
    'PROPFIND'  => :ok,
    'PROPPATCH' => :ok,
    'MKCOL'     => :created,
    'COPY'      => :no_content,
    'MOVE'      => :no_content,
    'LOCK'      => :created,
    'UNLOCK'    => :no_content,

    # RFC3253
    'VERSION-CONTROL'  => :ok,
    'REPORT'           => :multi_status,
    'CHECKOUT'         => :ok,
    'CHECKIN'          => :created,
    'UNCHECKOUT'       => :ok,
    'MKWORKSPACE'      => :created,
    'UPDATE'           => :multi_status,
    'LABEL'            => :ok,
    'MERGE'            => :multi_status,
    'BASELINE-CONTROL' => :ok,
    'MKACTIVITY'       => :created,

    # RFC3648
    'ORDERPATCH' => :ok,

    # RFC3744
    'ACL' => :ok,

    # RFC5323
    'SEARCH' => :multi_status,  # Multi Status

    # RFC5789
    'PATCH'   => :no_content
  }

  class Capturer
    TRAP_ID = 'X-Trap-Id'

    def initialize(app)
      @app = app
    end

    def call(env)
      @env, @status, @header, @rack_body_proxy = env, *@app.call(env)

      if @trap_id = @header.delete(TRAP_ID)
        set_custom_header!
        capture!
      end

      [@status, @header, @rack_body_proxy]
    end

    def set_custom_header!
      if @env['REQUEST_METHOD'] == 'OPTIONS'
        @header['Allow'] = ActionDispatch::Request::HTTP_METHODS.join(',')
      end

      if @env['REQUEST_METHOD'] == 'TRACE'
        @header['Content-Type'] = 'message/http'
      end
    end

    def capture!
      Request.create!(
        trap_id: @trap_id,
        headers: {
          request: {
            date:         Time.now.httpdate,
            scheme:       @env['rack.url_scheme'],
            query_params: @env['rack.request.query_hash'],
            body_params:  @env['rack.request.form_hash'],
            header:       @env.select { |key, _| key[/\A[A-Z0-9_]*\Z/] }
          },
          response: {
            status: @status,
            date:   Time.now.httpdate,
            header: response_header
          }
        }
      )
    end

    def response_header
      unless Rack::Utils::STATUS_WITH_NO_ENTITY_BODY.include?(@status)
        return @header.dup.update({'Content-Length' => body_bytesize.to_s})
      end
      @header
    end

    def body_bytesize
      @rack_body_proxy.map(&:bytesize).sum
    rescue NoMethodError
      # sometimes Rack::BodyProxy does not respond to #map
      bytesize = 0
      @rack_body_proxy.each { |part| bytesize += part.bytesize }
      bytesize
    end
  end

  belongs_to :trap
end
