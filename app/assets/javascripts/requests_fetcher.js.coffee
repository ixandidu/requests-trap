jQuery ->
  $requestsContainer = $('#requests-container')
  $requestsCount     = $('#requests-count')
  if $requestsContainer.length
    FAST_INTERVAL = 10000
    SLOW_INTERVAL = 60000
    FAIL_MESSAGE  = "Failed to update received requests in realtime, retrying in #{SLOW_INTERVAL/1000} seconds..."

    updateReceivedRequests = ->
      initialRequestCount = $requestsCount.text()
      $requestsCount.text('fetching...')
      $.getScript(
        document.location.href
      ).done((script, textStatus) ->
        if textStatus is 'success'
          setTimeout updateReceivedRequests, FAST_INTERVAL
      ).fail((jqxhr, textStatus, exception) ->
        $requestsCount.text(initialRequestCount)
        console.log FAIL_MESSAGE
        setTimeout updateReceivedRequests, SLOW_INTERVAL
      )

    setTimeout updateReceivedRequests, FAST_INTERVAL
