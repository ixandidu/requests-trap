class CreateRequests < ActiveRecord::Migration
  def change
    create_table :requests do |t|
      t.references :trap,      index: true
      t.json       :headers, default: {}

      t.timestamps
    end
  end
end
