class CreateTraps < ActiveRecord::Migration
  def change
    create_table :traps do |t|
      t.string :name
      t.string :slug

      t.timestamps
    end

    add_index :traps, :slug, unique: true
  end
end
